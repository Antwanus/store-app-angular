import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";
import { Supplier } from '../models/Supplier';
import { Product } from '../models/Product';

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/hal+json",
    "X-Forwarded-Host": "localhost",
    "X-Forwarded-Port": "4200"
  })
};

@Injectable({
  providedIn: 'root'
})
export class SupplierService {
  suppliersUrl: string = "api/suppliers";

  constructor(private http: HttpClient) { }

  // GET ALL SUPPLIERS
  getAllSuppliers(): Observable<Supplier[]> {
    return this.http
      .get(this.suppliersUrl, httpOptions)
      .pipe(map((result: any) => {
        return result._embedded.suppliers;
      }));
  }
  // CREATE SUPPLIER
  createSupplier(supplier: Supplier) {
    return this.http.post(this.suppliersUrl, supplier, httpOptions);
  }
  // GET SINGLE SUPPLIER
  getSingleSupplier(supplier: Supplier): Observable<Supplier> {
    const url: string = `${this.suppliersUrl}/${supplier.supplierId}`;
    return this.http
      .get(url, httpOptions)
      .pipe(map((result: any) => {
        return result;
      }));
  }
  // GET SUPPLIER-PRODUCTS
  getSupplierProducts(supplier: Supplier): Observable<Product[]>{
    const url: string = `${this.suppliersUrl}/${supplier.supplierId}/products`;
    return this.http
      .get(url, httpOptions)
      .pipe(map((result: any) => {
        return result._embedded.products;
      }));
  }
  /**  DELETE SUPPLIER BY HREF
   *   
   * 
  */
  deleteSupplier(href: string) {
    return this.http
      .delete(href, httpOptions);
  }

}
