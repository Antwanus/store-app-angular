import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable  } from "rxjs";

import { Customer } from "../models/Customer";

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/hal+json",
    "X-Forwarded-Host": "localhost",
    "X-Forwarded-Port": "4200"
  })
};

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  customersUrl: string = "api/customers";

  constructor(
    private http: HttpClient
  ) { }

  // GET ALL CUSTOMERS
  getAllCustomers(): Observable<Customer[]> {
    return this.http
        .get(this.customersUrl, httpOptions)
        .pipe(map((result: any) => {
          return result._embedded.customers;  
        }));
  }

  // CREATE A CUSTOMER
  createCustomer(customer: Customer): Observable<Customer> {
    return this.http.post(this.customersUrl, customer, httpOptions);
  }
  
}
