import { Component, OnInit } from '@angular/core';

import { ProductService } from "../../services/product.service";
import { Product } from "../../models/Product";

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  product: Product;
  productForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private productService: ProductService,
  ) {
    this.productForm = this.formBuilder.group({
      productName: '',
      pricePerUnit: '',
      sold: ''
    });
  }
  ngOnInit() {
    //TODO flashmsg to welcome & instruct the user
  }

  // SUBMIT FORM
  onSubmit() {
    // wire Product to formdata
    this.product = this.productForm.value;
    // reset the form to initial value (in constructor)
    this.productForm.reset();
    // call service to handle the POST request to the API
    this.productService
            .createProduct(this.product)
            .subscribe();        
    //TODO replace with custom flashmsg & maybe use the response to return the new ID of the product
    window.alert("Sending data to the DB...");
  }


}
