import { Component, OnInit, ViewChild } from '@angular/core';
import { Supplier } from 'src/app/models/Supplier';
import { SupplierService } from 'src/app/services/supplier.service';

import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatButton } from '@angular/material';

import { faEye, faTruck, faTools, faTrashAlt } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-supplier-list',
  templateUrl: './supplier-list.component.html',
  styleUrls: ['./supplier-list.component.css']
})
export class SupplierListComponent implements OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Supplier>;

  // MATERIAL
  matButton = MatButton;
  // ICONS
  faEye = faEye;
  faTruck = faTruck;
  faTools = faTools;
  faTrashAlt = faTrashAlt;
  // TYPES
  suppliers: Supplier[];
  sortedSuppliers: Supplier[];
  // DATASOURCE & MATERIAL
  dataSourceSupplier = new MatTableDataSource<Supplier>();
  displayedColumns = ['name', 'account', 'action'];

  constructor(
    private supplierService: SupplierService,
    public dialog?: MatDialog,
  ) { }

  // LIFECYCLE-HOOKS
  ngOnInit() {
    this.getSupplierList();
  }
  ngAfterViewInit(): void {
    this.dataSourceSupplier.sort = this.sort;
    this.dataSourceSupplier.paginator = this.paginator;
  }
  // FUNCTIONS
  /** Get all suppliers & bind them to 'datasource' */
  public getSupplierList = () => {
    this.supplierService
      .getAllSuppliers()
      .subscribe(res => {
        this.suppliers = res;
        this.dataSourceSupplier.data = res as Supplier[];
      });
  }
  /** DUMMY CLICK */
  click() {
    console.log(this);

  }
  /** Delete supplier & notify user */
  deleteClick(supplier: Supplier): void {
    console.log(supplier._links.self.href);
    
    this.supplierService
      .deleteSupplier(supplier._links.self.href)
      .subscribe();
    // REFRESH GET REQUEST
    this.getSupplierList();
  }
  doFilter = (value: string) => {
    this.dataSourceSupplier.filter = value.trim().toLowerCase();
  }
}
