import { Component, OnInit } from '@angular/core';

import { SupplierService } from "../../services/supplier.service";
import { Supplier } from "../../models/Supplier";

import { FormBuilder, FormGroup } from "@angular/forms";

@Component({
  selector: 'app-supplier-form',
  templateUrl: './supplier-form.component.html',
  styleUrls: ['./supplier-form.component.css']
})
export class SupplierFormComponent implements OnInit {

  supplier: Supplier;
  supplierForm: FormGroup;

  constructor(
    private supplierService: SupplierService,
    private formBuilder: FormBuilder,
  ) {
    this.supplierForm = this.formBuilder.group({
      supplierName: '',
    });
  }

  ngOnInit() {
    //TODO Welcome & instructing msgFlash
  }

  // SUBMIT FORM
  onSubmit() {
    // wire Supplier to form data
    this.supplier = this.supplierForm.value;
    // reset the form to initial value (in constructor)
    this.supplierForm.reset();
    // call service for POST
    this.supplierService
            .createSupplier(this.supplier)
            .subscribe();

  }

}
