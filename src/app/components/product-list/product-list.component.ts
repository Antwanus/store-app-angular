import { Component, OnInit, ViewChild } from '@angular/core';
import { Product } from 'src/app/models/Product';
import { ProductService } from 'src/app/services/product.service';

import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatButton } from '@angular/material';

import { faEye, faTruck, faTools, faTrashAlt } from "@fortawesome/free-solid-svg-icons";


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Product>;

  // MATERIAL
  matButton = MatButton;
  // ICONS
  faEye = faEye;
  faTruck = faTruck;
  faTools = faTools;
  faTrashAlt = faTrashAlt;
  // TYPES
  products: Product[];
  sortedProducts: Product[];
  dataSourceProducts = new MatTableDataSource<Product>();
  displayedColumns = ['productName', 'pricePerUnit', 'action'];

  // CONSTRUCTOR
  constructor(
    private productService: ProductService,
    public dialog?: MatDialog
  ) { }

  // LIFECYCLE-HOOKS
  ngOnInit() {
    this.getProductList();
  }
  ngAfterViewInit(): void {
    this.dataSourceProducts.sort = this.sort;
    this.dataSourceProducts.paginator = this.paginator;
  }
  // FUNCTIONS
  /** Get all products & bind them to 'datasource' for material */
  public getProductList = () => {
    this.productService
      .getAllProducts()
      .subscribe(res => {
        this.products = res;
        this.dataSourceProducts.data = res as Product[];
      });
  }
  /** DUMMY CLICK */
  click() {
  }
  /** Delete product & notify user 
   *  
   * Can't delete the product if a supplier still has this
   * listed :)
   * TODO: Error -> notify user
   * 
  */
  clickDelete(product: Product): void {
    console.log(product);
    
    this.productService
        .deleteProductByHref(product)
        .subscribe();

    // REFRESH THE PAGE BY SENDING ANOTHER GET
    this.getProductList();
    //TODO Delete success notification
  }

  doFilter = (value: string) => {
    this.dataSourceProducts.filter = value.trim().toLowerCase();
  }
}