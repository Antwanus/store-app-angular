import { Component, OnInit, ViewChild } from '@angular/core';
import { Customer } from "../../models/Customer";
import { CustomerService } from "../../services/customer.service";

import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatButton } from '@angular/material';

import { faEye, faTruck, faTools, faTrashAlt } from "@fortawesome/free-solid-svg-icons";


@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Customer>;
  
  // MATERIAL
  matButton = MatButton;
  // ICONS
  faEye = faEye;
  faTruck = faTruck;
  faTools = faTools;
  faTrashAlt = faTrashAlt;
  // TYPES
  customers: Customer[];
  sortedCustomers: Customer[];
  private dataSource = new MatTableDataSource<Customer>();
  displayedColumns = ['name', 'account', 'action'];
  
  constructor(private customerService: CustomerService) { }

  // LIFECYCLES
  ngOnInit() {
  this.getCustomerList();
  }
  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  // FUNCTIONS
  /** Get all customers & bind them to 'datasource' for material */
  public getCustomerList = () => {
    this.customerService
      .getAllCustomers()
      .subscribe(res => {
        this.customers = res;
        this.dataSource.data = res as Customer[];
      })
  }
  /** DUMMY CLICK */
  click(){
    console.log(this);
    
  }
  /** DELETE customer & notify user 
   * 
   *  Maybe add property to entity -> boolean 'isBanned' 
   * instead of deleting a customer
  */
  deleteClick() {
    console.log(this);
  }
  doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLowerCase();
  } 
}

