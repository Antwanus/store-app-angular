import { Component, OnInit } from '@angular/core';
import { faCoffee } from "@fortawesome/free-solid-svg-icons";
import { NgbDropdown } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  faCoffee = faCoffee;
  constructor() { }

  ngOnInit() {
  }

}
