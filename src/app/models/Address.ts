export class Address {
    name: string;
    street: string;
    streetnumber: string;
    zip: string;
    city: string;
    _links: {
        self: {
            href: string;
        },
        address: {
            href: string;
        },
        customer: {
            href: string;
        },
        supplier: {
            href: string;
        }
    }
}