export class BankAccount {
    accountHolder: string;
    accountNumber: number;
    _links: {
      self: {
        href: string;
      },
      bankAccount: {
        href: string;
      },
      supplier: {
        href: string;
      },
      customer: {
        href: string;
      }
    };
}