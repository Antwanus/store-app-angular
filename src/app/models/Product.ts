export class Product {
    productName?: string;
    pricePerUnit?: number;
    created?: string;
    updated?: string;
    sold?: string;
    productId?: number;
    _links?: {
        self?: {
            href?: string;
        },
        product?: {
            href?: string;
        },
        supplier?: {
            href?: string;
        }
    }
}