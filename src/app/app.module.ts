import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { MatPaginatorModule } from "@angular/material";
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from "@angular/material/table";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";

import { MatInputModule } from "@angular/material";
import { MatDialogModule } from "@angular/material/dialog";
import { MatButtonModule } from "@angular/material/button";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductListComponent } from './components/product-list/product-list.component';
import { HeaderComponent } from './header/header/header.component';
import { NavbarComponent } from './header/navbar/navbar.component';
import { FooterComponent } from './footer/footer/footer.component';
import { WelcomeComponent } from './components/pages/welcome/welcome.component';
import { SupplierListComponent } from './components/supplier-list/supplier-list.component';
import { CustomerListComponent } from './components/customer-list/customer-list.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { CustomerFormComponent } from './components/customer-form/customer-form.component';
import { SupplierFormComponent } from './components/supplier-form/supplier-form.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    HeaderComponent,
    NavbarComponent,
    FooterComponent,
    WelcomeComponent,
    SupplierListComponent,
    CustomerListComponent,
    ProductFormComponent,
    CustomerFormComponent,
    SupplierFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,

    MatTableModule,
    MatSortModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatCheckboxModule,
    
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    HttpClientModule
  ],
  exports: [NgbModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
