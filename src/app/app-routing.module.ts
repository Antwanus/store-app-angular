import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WelcomeComponent } from './components/pages/welcome/welcome.component';

import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductFormComponent } from './components/product-form/product-form.component';

import { SupplierListComponent } from './components/supplier-list/supplier-list.component';
import { SupplierFormComponent } from './components/supplier-form/supplier-form.component';

import { CustomerListComponent } from './components/customer-list/customer-list.component';
import { CustomerFormComponent } from './components/customer-form/customer-form.component';


const routes: Routes = [
  { path: "", component: WelcomeComponent },
  { path: "product-list", component: ProductListComponent },
  { path: "product-form", component: ProductFormComponent },
  { path: "supplier-list", component: SupplierListComponent },
  { path: "supplier-form", component: SupplierFormComponent },
  { path: "customer-list", component: CustomerListComponent },
  { path: "customer-form", component: CustomerFormComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
